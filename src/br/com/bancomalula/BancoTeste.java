package br.com.bancomalula;

public class BancoTeste {

	public static void main(String[] args) {
		
		Conta.saldoDoBanco = 2_000_000.00;
		
		Conta conta = new Conta(1, "Corrente", 2_750.00, "123!", new Cliente("Jorge", "133214234", 
				"12342141452", "jorge@senai.com", "Masculino"));
		
		conta.exibeSaldo();
		conta.deposita(50);
		conta.exibeSaldo();
		System.out.println("Cofre: "+ Conta.saldoDoBanco);
		conta.saca(100);
		conta.exibeSaldo();
		System.out.println("Cofre: "+ Conta.saldoDoBanco);
		
		System.out.println("DADOS DA CONTA");
		System.out.println("N� "+ conta.getNumero());
		System.out.println("Tipo: "+ conta.getTipo());
		System.out.println("Senha: "+ conta.getSenha());
		System.out.println("Saldo: "+ conta.getSaldo());
		System.out.println("T�tular: "+ conta.getCliente(). getNome());
		System.out.println("CPF: "+ conta.getCliente(). getCpf());
		System.out.println("RG: "+ conta.getCliente(). getRg());
		System.out.println("E-mail: "+ conta.getCliente(). getEmail());
		System.out.println("Sexo: "+ conta.getCliente(). getSexo());



	}

}
